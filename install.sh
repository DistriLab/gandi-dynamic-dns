#!/bin/bash 

cp usr/local/bin/live-dns-update.sh /usr/local/bin/live-dns-update.sh
chmod +x /usr/local/bin/live-dns-update.sh

mkdir /etc/live-dns
cp etc/live-dns/live-dns.conf /etc/live-dns/live-dns.conf
chmod 400 /etc/live-dns/live-dns.conf

cp etc/cron.d/live-dns-update /etc/cron.d/live-dns-update

echo You now need to set your configuration in the file /etc/live-dns/live-dns.conf
